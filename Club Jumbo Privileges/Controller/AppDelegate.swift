//
//  AppDelegate.swift
//  Club Jumbo Privileges
//
//  Created by Gobi R. on 18/04/18.
//

import UIKit
import CoreData
import UserNotifications
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var uuid = String()
    var count = 1
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.uuid = UIDevice.current.identifierForVendor!.uuidString
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        UIApplication.shared.statusBarStyle = .lightContent
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        checkDataBase()
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        FirebaseApp.configure()
        NotificationCenter.default.addObserver(self,selector: #selector(self.tokenRefreshNotification),
                                               name: .InstanceIDTokenRefresh,
                                               object: nil)
       
        return true
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print(fcmToken)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        UIApplication.shared.applicationIconBadgeNumber += count
        completionHandler([.alert, .sound])
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let actionIdentifier = response.actionIdentifier
        switch actionIdentifier {
        case UNNotificationDismissActionIdentifier: break
        case UNNotificationDefaultActionIdentifier:
            navigateToWebpage()
        break
        default:
            break
        }
}
    
    func saveToken(){
        if let refreshedToken = InstanceID.instanceID().token() {
            //  let id = InstanceID.instanceID().token()
            print("InstanceID token: \(refreshedToken)")
            let defaults = UserDefaults.standard
            defaults.set(refreshedToken, forKey: "FCMToken")
            //  print("InstanceID token: \(String(describing: id))")
            //SharedVariables.sharedInstance.GcmId = (id)!
            
            let myUrl = URL(string: "https://jumbo.mu/savefcm");
            
            var request = URLRequest(url:myUrl!)
            
            request.httpMethod = "POST"
            
            let postString = "user_id=\(uuid)&fcm=\(refreshedToken)&platform=iOS";
            
            request.httpBody = postString.data(using: String.Encoding.utf8);
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                
                if error != nil
                {
                    print("error=\(String(describing: error))")
                    return
                }
                //Let's convert response sent from a server side script to a Dictionary object:
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any]
                    if let parseJSON = json {
                        print(parseJSON)
                    }
                } catch {
                    print(error)
                }
            }
            task.resume()
        }else{
            print(" InstanceID gain called ")
            connectToFcm()
        }
    }
    
    @objc func tokenRefreshNotification(_ notification: Notification) {
            saveToken()
            connectToFcm()
            // Connect to FCM since connection may have failed when attempted before having a token.
            
        }
        
        func connectToFcm() {
            guard InstanceID.instanceID().token() != nil else {
                connectToFcm()
                return
            }
            Messaging.messaging().shouldEstablishDirectChannel = false
            Messaging.messaging().shouldEstablishDirectChannel = true
            saveToken()
            
            //        Messaging.messaging().connect { (error) in
            //            if error != nil {
            //                print("Unable to connect with FCM. \(String(describing: error))")
            //            } else {
            //                print("Connected to FCM.")
            //            }
            //        }
        }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // handle your message
        // Let FCM know about the message for analytics etc.
        Messaging.messaging().appDidReceiveMessage(userInfo)
        var currentBadgeNumber: Int = application.applicationIconBadgeNumber
        currentBadgeNumber += Int(userInfo["badge"] as? String ?? "0")!
        application.applicationIconBadgeNumber = currentBadgeNumber
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken as Data
        
        print("\(deviceToken)")
        // let token = FIRInstanceID.instanceID().token()
        //print("tokkeeeeeen",token!)
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token1: \(refreshedToken)")
            let defaults = UserDefaults.standard
            defaults.set(refreshedToken, forKey: "FCMToken")
            
        }
        Messaging.messaging()
            .setAPNSToken(deviceToken, type: MessagingAPNSTokenType.sandbox)
        Messaging.messaging()
            .setAPNSToken(deviceToken, type: MessagingAPNSTokenType.prod)
        //   InstanceID.instanceID().setAPNSToken(deviceToken as Data, type: InstanceIDAPNSTokenType.sandbox)
        //        let DeviceToken = UIDevice.current.identifierForVendor
        //        print(DeviceToken!)
    }

    
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Club_Jumbo_Privileges")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func checkDataBase() {
        let fileManager = FileManager.default
        let strDbPath = getDbPath()
        var defaultPath = URL(fileURLWithPath: Bundle.main.resourcePath!).absoluteString
        if fileManager.fileExists(atPath: strDbPath) == false {
            
             defaultPath = URL(fileURLWithPath: Bundle.main.resourcePath!).appendingPathComponent("codebar.sqlite").absoluteString
            print(defaultPath)
            
        } else {
            do {
                try fileManager.copyItem(atPath: defaultPath, toPath: strDbPath)
                
            } catch let error as NSError {
                print("Couldn't copy file to final location! Error:\(error.description)")
            }
        }
        
    }
    
    func getDbPath() -> String {
        
        let arryPaths = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        print(arryPaths)
        let documentDirStr: String = arryPaths.appendingPathComponent("codebar.sqlite").absoluteString
        print(documentDirStr)
        return documentDirStr
    }
    
    func navigateToWebpage() {
        UIApplication.shared.applicationIconBadgeNumber = 0
        count = 1
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nosPromosVc = storyBoard.instantiateViewController(withIdentifier: "WebViewJnViewController") as! WebViewJnViewController
        nosPromosVc.url = constant.promotionsURL
        nosPromosVc.pageTitle = constant.clubJumbo
        let navigationControler = self.window?.rootViewController as! UINavigationController
        navigationControler.pushViewController(nosPromosVc, animated: false)
    }
}

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
    
    
}
