//
//  BarcodeInput.swift
//  Club Jumbo Privileges
//
//  Created by Vengatesh C. on 19/04/18.
//

import UIKit

class BarcodeInput: MenuDisplayControllerViewController {

    @IBOutlet weak var toScannerViewOutlet: UIButton!
    @IBOutlet weak var toPersonalisationOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        toScannerViewOutlet.imageView?.contentMode = .scaleToFill
        toPersonalisationOutlet.imageView?.contentMode = .scaleToFill
        backButton()
        toScannerViewOutlet.layer.cornerRadius = 10
        toPersonalisationOutlet.layer.cornerRadius = 10
        customNavigationBar()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
         self.backButton()
          self.customNavigationBar()
        }
    }
    func customNavigationBar()
    {
        self.navigationController?.navigationBar.topItem?.title = "";
        self.navigationItem.title = "Je Saisis ma Carte"
    }
    
    @objc func sidemenu(sender:UIButton)
    {
        self.openVCSlideMenuButtonPressed(sender as UIButton)
        print("Slidemenu Clicked")
    }

    @IBAction func toScannerView(_ sender: UIButton) {
        
        let storyBoard  = UIStoryboard(name: "Main", bundle: nil)
        let scanView = storyBoard.instantiateViewController(withIdentifier: "ScannerViewController") as! ScannerViewController
        navigationController?.pushViewController(scanView, animated: true)
    }
    
    
    @IBAction func toPersonalisationView(_ sender: UIButton) {
        
        let pernoanlView = storyboard?.instantiateViewController(withIdentifier: "PersonalisationViewController") as! PersonalisationViewController
        navigationController?.pushViewController(pernoanlView, animated: true)
    }
    
}
