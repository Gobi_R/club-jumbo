//
//  Constants.swift
//  Club Jumbo Privileges
//
//  Created by Gobi R. on 19/04/18.
//

import UIKit
import CoreData
import AVFoundation

struct constant {
    
    // MARK: App Web URLS
    static let baseUrl = "https://jumbo.mu/savefcm"
    static let fidalityCardSignupURL = "https://jumbo.mu/subscriberminipage?email=1"
    static let promotionsURL = "https://jumbo.mu/promominipage"
    static let contatctFormURl = "https://jumbo.mu/contactminipage"
    static let magasinsURL = "https://jumbo.mu/magasinminipage"
    /* if neede to download pdf from magasins page */
    //   static let downloadURL = "http://s3.amazonaws.com/online.fliphtml5.com/vcsy/euom/euom.pdf?AWSAccessKeyId=AKIAJHMQTBN5CJM6PDBQ&Expires=1524808109&Signature=SoJC052wkWRccrKkW5OahEhVk9s%3D"
   
    // MARK: App NavigationBar Titles
    static let fidalityCardInputTitle = "Ma Carte Privilègs"
    static let barcodeInputTitle = "Je saisis ma Carte"
    static let fidalityCardSignupTitle = "Je m'incris"
    static let promotionsTitle = "Nos Promotions"
    static let contatcFormTitle = "Formulaire de contact"
    static let magasinsTitle = "Nos Magasins"
    static let mentionLegalesTitle = "Mentions Lègales"
    static let aproposTitle = "A Propos"
    static let clubJumbo = "Club Jumbo Privilèges"
    
    
    //MARK: CoreImage Fetched
    static var barcodeImageFile = UIImage()
    
    //MARK: CoreString Fetched
    static var coreBarcodeString = [String]()
    
    static var player: AVAudioPlayer?
    
    static var whichOne = 0
    
     static var docOrHtml: Bool!
    
}

extension Notification.Name {
    static let postNotifi = Notification.Name("postNotifi")
}

//extension UITableView {
//    func scrollToBottom(animated: Bool = true) {
//        let sections = self.numberOfSections
//        let rows = self.numberOfRows(inSection: sections - 1)
//        if (rows > 0){
//            self.scrollToRowAtIndexPath(indexpath(forRow: rows - 1, inSection: sections - 1), atScrollPosition: .Bottom, animated: true)
//        }
//    }
//}

extension UIViewController
{
    
    func backButton()
    {
        
        let button1 = UIBarButtonItem(image: UIImage(named: "arrow_back_w"), style: .plain, target: self, action: #selector(self.popView))
        
        self.navigationItem.leftBarButtonItem  = button1
    }
    
//    func navigationtitle(title:String)
//    {
//        self.navigationController?.navigationBar.topItem?.title = "";
//        self.navigationItem.title = title
//        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "arrow_back_w")
//
//    }
    
    @objc func popView() {
       navigationController?.popViewController(animated: true)
    }
    
    func playSound(done: Bool) {
        guard let url = Bundle.main.url(forResource: "beep-6", withExtension: "wav") else {
            print("url not found")
            return
        }
        
        do {
            /// this codes for making this app ready to takeover the device audio
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /// change fileTypeHint according to the type of your audio file (you can omit this)
            
            constant.player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.wav.rawValue)
            
            // no need for prepareToPlay because prepareToPlay is happen automatically when calling play()
            if done == true {
                constant.player!.play()
            }
        } catch let error as NSError {
            print("error: \(error.localizedDescription)")
        }
    }
    
}






