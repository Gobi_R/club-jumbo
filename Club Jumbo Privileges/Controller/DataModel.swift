//
//  DataModel.swift
//  Club Jumbo Privileges
//
//  Created by Gobi R. on 24/04/18.
//

import UIKit
import CoreData
import CoreImage
import SQLite3

class DataModel {
    
    let SQLITE_STATIC = unsafeBitCast(0, to: sqlite3_destructor_type.self)
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var db: OpaquePointer?
    var stmt: OpaquePointer? = nil
    var insertSql = String()
    func insertData() {
       
        if sqlite3_open(appDelegate.getDbPath(), &db) == SQLITE_OK {
           if stmt == nil {
            if sqlite3_exec(db, "create table if not exists student (id integer primary key, name varchar(150), code varchar(150), image blob)", nil, nil, nil) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db)!)
                print("error creating table: \(errmsg)")
            }
                insertSql = "INSERT OR REPLACE INTO student(id, name, code, image) VALUES (?,?,?,Null)"
                if sqlite3_prepare_v2(db, insertSql, -1, &stmt, nil) == SQLITE_OK {
                    let code: NSString = UserDefaults.standard.value(forKey: "barcodeString") as! NSString
                    
                    if sqlite3_bind_text(stmt, 1, code.utf8String, -1, nil) != SQLITE_OK {
                        let errmsg = String(cString: sqlite3_errmsg(db))
                        print(errmsg)
                    }
                    if sqlite3_bind_text(stmt, 2, "", -1, nil) != SQLITE_OK {
                        let errmsg = String(cString: sqlite3_errmsg(db))
                        print(errmsg)
                    }
                    if sqlite3_bind_text(stmt, 3, code.utf8String, -1, nil) != SQLITE_OK {
                        let errmsg = String(cString: sqlite3_errmsg(db))
                        print(errmsg)
                    }
                    if sqlite3_bind_blob(stmt, 4, nil, -1, nil) != SQLITE_OK {
                        let errmsg = String(cString: sqlite3_errmsg(db))
                        print(errmsg)
                    }
                    if sqlite3_step(stmt) != SQLITE_DONE {
                        let errmsg = String(cString: sqlite3_errmsg(db))
                        print(errmsg)
                    }
                    if sqlite3_finalize(stmt) != SQLITE_OK {
                        let errmsg = String(cString: sqlite3_errmsg(db))
                        print(errmsg)
                    }
                    stmt = nil
                    if sqlite3_close(db) != SQLITE_OK {
                        print("Error Closing Database")
                    }
                    db = nil
                } else {
                    let errmsg = String(cString: sqlite3_errmsg(db))
                    print(errmsg)
                }
        } else {
             print("error opening database")
        }
    }
    }
    func retriveData() {
       
        if sqlite3_open(appDelegate.getDbPath(), &db) == SQLITE_OK {
             let key = UserDefaults.standard.value(forKey: "barcodeString") as? String
            if key != nil {
        if sqlite3_prepare_v2(db, "SELECT *FROM student WHERE id = \(key!)", -1, &stmt, nil) == SQLITE_OK {
            while sqlite3_step(stmt) == SQLITE_ROW {
                let id = sqlite3_column_int64(stmt, 0)
                let id1 = sqlite3_column_text(stmt, 0)!
                print("id = \(id)")
                let str = String(cString: id1)
                print(str)
                constant.barcodeImageFile = fromString(string: str)!
            }
        } else {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print(errmsg)
                } }}
    }
    
    func checkEmptyDatabase() -> Int64? {
         let key = UserDefaults.standard.value(forKey: "barcodeString") as? String
        var id = sqlite_int64()
        if key != nil {
        if sqlite3_prepare_v2(db, "SELECT *FROM student WHERE id = \(key!)", -1, &stmt, nil) == SQLITE_OK {
            while sqlite3_step(stmt) == SQLITE_ROW {
                 id = sqlite3_column_int64(stmt, 0)
                print("id = \(id)")
                
            }
            }}
        return id
    }
    
    func deleteSql() {
        let key = UserDefaults.standard.value(forKey: "barcodeString") as! String
        if sqlite3_prepare_v2(db, "DELETE FROM student WHERE Id = \(key)", -1, &stmt, nil) == SQLITE_OK {
           if sqlite3_step(stmt) == SQLITE_DONE {
                print("Deleted SuccessFully")
            }
        } else {
            print("prepare to delete Failssss.")
        }
    }
    func fromString(string : String) -> UIImage? {

        let data = string.data(using: .ascii)
        let filter = CIFilter(name: "CICode128BarcodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        let qrcodeCIImage = filter?.outputImage!
        let cgImage = CIContext(options:nil).createCGImage(qrcodeCIImage!, from: (qrcodeCIImage?.extent)!)
        UIGraphicsBeginImageContext(CGSize(width: 200 * UIScreen.main.scale, height:200 * UIScreen.main.scale))
        let context = UIGraphicsGetCurrentContext()
        context!.interpolationQuality = .none
        context?.draw(cgImage!, in: CGRect(x: 0.0,y: 0.0,width: context!.boundingBoxOfClipPath.width,height: context!.boundingBoxOfClipPath.height))
        let preImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let qrCodeImage = UIImage(cgImage: (preImage?.cgImage!)!, scale: 1.0/UIScreen.main.scale, orientation: .downMirrored)
        return qrCodeImage
    }
}


