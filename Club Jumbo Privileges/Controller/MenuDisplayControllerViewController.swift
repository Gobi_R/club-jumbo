//
//  MenuDisplayControllerViewController.swift
//  XTRM
//
//  Created by Karthikeyan A. on 25/01/18.
//  Copyright © 2018 DCI. All rights reserved.
//

import UIKit

class MenuDisplayControllerViewController: UIViewController,SlideMenuDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func openVCSlideMenuButtonPressed(_ sender : UIButton){
       
        if (sender.tag == 10)
        {
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
             let viewMenuBack : UIView = view.subviews.last!
             UIView.animate(withDuration: 1.0, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = 10 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC: MenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        menuVC.view.backgroundColor = UIColor.clear
        let height = self.navigationController?.navigationBar.frame.height
        print(height!)
        print(UIScreen.main.bounds.size.width)
        menuVC.view.frame=CGRect(x:UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion: { (finished: Bool) in
            
            menuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        })
    }

    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        let topViewController : UIViewController = self.navigationController!.topViewController!
        print("View Controller is : \(topViewController) \n", terminator: "")
        switch(index){
        case 0:
            print("Ma Carte Privilèges")
            self.openViewControllerBasedOnIdentifier("ViewController", ind: index)
            break
       
        case 1, 2, 3, 5, 6:
            print("Nos Promotions")
            self.openViewControllerBasedOnIdentifier("WebViewJnViewController", ind: index)
            break
//        case 2:
//            print("Nos Magasins")
//            self.openViewControllerBasedOnIdentifier("WebViewJnViewController")
//            break
//        case 3:
//             print("Nos Contacter")
//               self.openViewControllerBasedOnIdentifier("WebViewJnViewController")
//            break
        case 4:
            print("Instructions")
            self.openViewControllerBasedOnIdentifier("InstructionPageView", ind: index)
            break
//     case 5:
//                 print("À propos")
//                 self.openViewControllerBasedOnIdentifier("AproposViewController", ind: index)
//            break
//        case 6:
//            print("Mentions lègales")
//            self.openViewControllerBasedOnIdentifier("MentionsLegelasViewController", ind: index)
//            break
            
        default:
            print("default\n", terminator: "")
        }
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String, ind: Int32){
        
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
         let topViewController : UIViewController = self.navigationController!.topViewController!

             if(destViewController.isKind(of: ViewController.self)) {
                  if(topViewController.isKind(of: ViewController.self)) {
                    return
                }
                let mainPage = destViewController as! ViewController
                navigationController?.pushViewController(mainPage, animated: true)
            }
             else if(destViewController.isKind(of: WebViewJnViewController.self)) {
                let promotions = destViewController as! WebViewJnViewController
                var getURL  = ""
                var getTitle = ""
                var getHtml = ""
                if ind == 1{
                    getURL = constant.promotionsURL
                    getTitle = constant.clubJumbo
                }else if ind == 2{
                    getURL = constant.magasinsURL
                    getTitle = constant.clubJumbo
                }else if ind == 3{
                    getURL = constant.contatctFormURl
                    getTitle = constant.clubJumbo
                } else if ind == 5 {
                   constant.docOrHtml = true
                    let url = Bundle.main.path(forResource: "About", ofType: "html")
                    let htmlString = try? String(contentsOfFile: url!, encoding: String.Encoding.utf8)
                    getHtml = htmlString!
                    getTitle = constant.aproposTitle
                } else  {
                   constant.docOrHtml = false
//                    let pdf = Bundle.main.url(forResource: "Mentions legales", withExtension: "docx")
                    let pdf = Bundle.main.path(forResource: "Mentions legales", ofType: "docx")
                   // let pdfString = try? String (contentsOfFile: pdf!, encoding: String.Encoding.utf8)
                    getHtml = pdf!
                    getTitle = constant.mentionLegalesTitle
                }
                promotions.url = getURL
                promotions.pageTitle = getTitle
                promotions.html = getHtml
                if(topViewController.isKind(of: WebViewJnViewController.self)) {
                 
                    let imageDataDict:[String: String] = ["url": getURL, "title":getTitle, "html":getHtml]
                    print(imageDataDict)
                    NotificationCenter.default.post(name: .postNotifi, object: nil, userInfo: imageDataDict)
                    return
                }
                navigationController?.pushViewController(promotions, animated: true)
             }
             else if(destViewController.isKind(of:InstructionPageView.self)) {
                let instructionView = destViewController as! InstructionPageView
              
                navigationController?.pushViewController(instructionView, animated: true)
        }
//             else if(destViewController.isKind(of:AproposViewController.self)) {
//                let transferVc = destViewController as! AproposViewController
//                navigationController?.pushViewController(transferVc, animated: true)
//        }
//             else if(destViewController.isKind(of:MentionsLegelasViewController.self)) {
//                let transferVc = destViewController as! MentionsLegelasViewController
//                navigationController?.pushViewController(transferVc, animated: true)
//        }

        
 
    }


}

