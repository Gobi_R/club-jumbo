//
//  MenuViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: MenuDisplayControllerViewController {
   
    
    var slideMenuList = ["Ma Carte Privilèges","Nos Promotions","Nos Magasins","Nous Contacter","Instructions","À propos","Mentions lègales"]
    /**
     *  Array to display menu options
     */
    @IBOutlet var tblMenuOptions : UITableView!
    @IBOutlet weak var slideTableView: UITableView!
    
    // Transparent button to hide menu
     @IBOutlet weak var CloseMenu: UIButton!
   
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    var imageArray = [UIImage(named:"cartepriv_icon_r"),UIImage(named:"nospromotion_ico_r"),UIImage(named:"magasin_r_s"),UIImage(named:"nous_contacter_r_s"),UIImage(named:"instructions_r_s"),UIImage(named:"apropos_r_s"),UIImage(named:"mentionslegales_r_s")]
    
    /**
     *  Array containing menu options
     */
    // var arrayMenuOptions = [Dictionary<String,String>]()
    
    
    /**
     *  Menu button which was tapped to display the menu
     */
    var btnMenu : UIButton!
    
    /**
     *  Delegate of the MenuVC
     */
    var delegate : SlideMenuDelegate?
    
    let numberOfMenu = 7
    let menuCellIdentifier = "cellMenu"
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tblMenuOptions.tableFooterView = UIView()
        tblMenuOptions.separatorInset = .zero
        tblMenuOptions.layoutMargins = .zero
        tblMenuOptions.alwaysBounceVertical = true
       
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
    }
    
    @IBAction func CloseMenuAction(_ button: UIButton) {
        
           btnMenu.tag = 0
        self.view.backgroundColor = UIColor.clear

        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.CloseMenu){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 1.3, animations: { () -> Void in
            self.view.frame = CGRect(x: 10*UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
        
           }
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        self.view.backgroundColor = UIColor.clear
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
       }
        
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.view.frame = CGRect(x:10 * UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
    }
    
   
    
    
}
extension MenuViewController : UITableViewDataSource, UITableViewDelegate{
    
    // MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return slideMenuList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: menuCellIdentifier)!
        
        cell.preservesSuperviewLayoutMargins = false
      //  cell.backgroundColor = UIColor.clear
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        let tableImages: UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        tableImages.image = imageArray[indexPath.row]!
        lblTitle.text = slideMenuList[indexPath.row]
      
              return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = indexPath.row
        
        self.onCloseMenuClick(btn)

    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if  ( UIDevice.current.model.range(of: "iPad") != nil) {
        let lastRowAtIndex = tableView.numberOfRows(inSection: 0)
        if indexPath.row == lastRowAtIndex - 1 {
//            tableView.scrollToBottom(animated: true)
//            reloadData()
            }
        }
    }
    
//    func reloadData(){
//    slideTableView.reloadData()
//        DispatchQueue.main.async {
//            let scrollPoint = CGPoint(x: 0, y: self.slideTableView.contentSize.height - self.slideTableView.frame.size.height)
//            self.slideTableView.setContentOffset(scrollPoint, animated: true)
//        }
    
        
//    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let tableviewHeight = tblMenuOptions.bounds.height
//        let cellHeight = tableviewHeight/7
        return 52.2
    }

    }
    
//extension UITableView {
//    func scrollToBottom(animated: Bool = true) {
//        let sections = self.numberOfSections
//        let rows = self.numberOfRows(inSection: sections + 1)
//        if (rows > 0){
//            self.scrollToRow(at: NSIndexPath(row: rows , section: sections - 1) as IndexPath, at: .bottom, animated: true)
//
//        }
//    }
//}

