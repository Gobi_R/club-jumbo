//
//  PersonalisationNameViewController.swift
//  Club Jumbo Privileges
//
//  Created by Gobi R. on 20/04/18.
//

import UIKit

class PersonalisationNameViewController: MenuDisplayControllerViewController,UITextFieldDelegate {

    @IBOutlet weak var registerNameOutlet: UIButton!
    @IBOutlet weak var barcodeDisplayOutlet: UIImageView!
    @IBOutlet weak var barcodeNumberDisplayOutlet: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    
     let model = DataModel()
    
    override func viewDidLoad() {
          customNavigationBar()
        super.viewDidLoad()
        backButton()
        registerNameOutlet.layer.cornerRadius = 5
        barcodeNumberDisplayOutlet.text = UserDefaults.standard.value(forKey: "barcodeString") as? String
        userNameTextField.delegate = self
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        model.loadContext()
        model.retriveData()
        keyboardResizing()
        barcodeDisplayOutlet.image = constant.barcodeImageFile
        barcodeDisplayOutlet.contentMode = .scaleToFill
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        barcodeDisplayOutlet.layer.magnificationFilter = kCAFilterNearest

    }
    

     //MARK:- Save Barcode and show it to main view

    @IBAction func saveBarcod(_ sender: UIButton) {
        userNameTextField.resignFirstResponder()
        if (userNameTextField.text?.isEmpty)! || (userNameTextField.text!.count < 2) {
            let alert = UIAlertController(title: "", message: "S'il vous plaît entrer le nom", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "D'accord", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else {
        UserDefaults.standard.set(userNameTextField.text!, forKey: "userName")
        let userName =  UserDefaults.standard.value(forKey: "userName")
        print(userName as! String)
        navigationController?.popToRootViewController(animated: true)
        }}
    
    
    func customNavigationBar()
    {
        self.navigationController?.navigationBar.topItem?.title = "";
        self.navigationItem.title = "Personnalisation"
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        userNameTextField.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userNameTextField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let charSet = CharacterSet.letters.inverted
        let compSepByCharInSet = string.components(separatedBy: charSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        let maxLength = 20
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        
        return string == numberFiltered && newString.length <= maxLength
    }
    
    // MARK:- Keyboard Popup Control
    func keyboardResizing() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  .UIKeyboardWillHide, object: nil)
        
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        var getBool = false
        if notification.name == .UIKeyboardWillShow{
            getBool = true
        }
        adjustingHeight(getBool, notification: notification)
    }
    
    func adjustingHeight(_ show:Bool, notification:Notification) {
        
        var userInfo = (notification as NSNotification).userInfo!
        let keyboardFrame:CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        let changeInHeight = (keyboardFrame.height ) * (show ? 1 : 0)
        var getvf = self.view.frame
        if show{
            let gettextFr = userNameTextField.frame
            
            if  ( UIDevice.current.model.range(of: "iPad") != nil) {
                getvf.origin.y =  (gettextFr.origin.y + (gettextFr.size.height * 1.5) ) - changeInHeight
            }else {
                getvf.origin.y =  gettextFr.origin.y - changeInHeight
            }
        }else{
            getvf.origin.y = (self.navigationController?.navigationBar.frame.size.height)! + UIApplication.shared.statusBarFrame.size.height
        }
        self.view.frame = getvf
        UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
}
