//
//  PersonalisationViewController.swift
//  Club Jumbo Privileges
//
//  Created by Gobi R. on 20/04/18.
//

import UIKit
import CoreData

class PersonalisationViewController: MenuDisplayControllerViewController,UITextFieldDelegate {
    
    @IBOutlet weak var enRegistrerButton: UIButton!
    @IBOutlet weak var barcodeStringTextField: UITextField!
    @IBOutlet var toolBarView: UIView!
    
    let model = DataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        barcodeStringTextField.delegate = self
        barcodeStringTextField.inputAccessoryView = toolBarView
        backButton()
        
        enRegistrerButton.layer.cornerRadius = 5
        
        self.navigationController?.navigationItem.hidesBackButton = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
//        if UIDevice.current.model.range(of: "iPad") != nil {
          keyboardResizing()
//        }
          customNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
     super.viewWillDisappear(animated)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let numSet = CharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: numSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        let maxLength = 14
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
       
        
        return string == numberFiltered && newString.length <= maxLength
    }
    
    func keyboardResizing() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  .UIKeyboardWillHide, object: nil)
        
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        var getBool = false
        if notification.name == .UIKeyboardWillShow{
            getBool = true
        }
        adjustingHeight(getBool, notification: notification)
    }
    
    
    
    func adjustingHeight(_ show:Bool, notification:Notification) {
        
        var userInfo = (notification as NSNotification).userInfo!
        let keyboardFrame:CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        let changeInHeight = (keyboardFrame.height ) * (show ? 1 : 0)
        var getvf = self.view.frame
        if show{
            let gettextFr = barcodeStringTextField.frame
          
            if  ( UIDevice.current.model.range(of: "iPad") != nil) {
                  getvf.origin.y =  (gettextFr.origin.y + (gettextFr.size.height * 1.3) ) - changeInHeight + toolBarView.frame.height * 2
                print(toolBarView.frame.height)
            }else {
        getvf.origin.y =  gettextFr.origin.y - changeInHeight
        }
        }else{
            getvf.origin.y = (self.navigationController?.navigationBar.frame.size.height)! + UIApplication.shared.statusBarFrame.size.height
        }
        self.view.frame = getvf
        UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func customNavigationBar()
    {
        self.navigationController?.navigationBar.topItem?.title = "";
        self.navigationItem.title = "Personnalisation"
    }
   
    // MARK:- Validating
    /* Validating The text and Genarate Barcode */
    @IBAction func genarateBarcodValues(_ sender: UIButton) {
        barcodeStringTextField.resignFirstResponder()
        if barcodeStringTextField.text == "" {
            let alert = UIAlertController(title: "", message: "s'il vous plaît entrer le numéro de code à barres", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "D'accord", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else if ((barcodeStringTextField.text?.count)! < 5 )  {
            let alert = UIAlertController(title: "", message: "Votre carte n'est pas valide. Merci d'utiliser uniquement une carte du Club Privilèges de Jumbo et Spar", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "D'accord", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
    }else {
        barcodeStringTextField.resignFirstResponder()
        UserDefaults.standard.setValue(barcodeStringTextField.text!, forKey: "barcodeString")
//        model.save(barcodeStringValue: barcodeStringTextField.text!)
//        constant.whichOne = 1
        model.insertData()
        let stroyBoard = UIStoryboard(name: "Main", bundle: nil)
        let personalName = stroyBoard.instantiateViewController(withIdentifier: "PersonalisationNameViewController") as! PersonalisationNameViewController
        navigationController?.pushViewController(personalName, animated: true)
        }}

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        barcodeStringTextField.resignFirstResponder()
    }
    //MARK: ToolBar Action
    @IBAction func toolBarAction(_ sender: UIButton) {
        barcodeStringTextField.endEditing(true)
    }
    
    
}
