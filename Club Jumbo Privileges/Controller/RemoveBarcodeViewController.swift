//
//  RemoveBarcodeViewController.swift
//  Club Jumbo Privileges
//
//  Created by Gobi R. on 20/04/18.
//

import UIKit

class RemoveBarcodeViewController: MenuDisplayControllerViewController {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var barcodeNumberLabel: UILabel!
    @IBOutlet weak var barcodeFinalImage: UIImageView!
    
    let model = DataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model.retriveData()
//        model.loadContext()
        self.navigationController?.navigationItem.title = "Personalisation"
        userNameLabel.text = UserDefaults.standard.value(forKey: "userName") as? String
        barcodeNumberLabel.text = UserDefaults.standard.value(forKey: "barcodeString") as? String
        barcodeFinalImage.contentMode = .scaleToFill
        barcodeFinalImage.image = constant.barcodeImageFile
        customNavigationBar()
        backButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        customNavigationBar()
    }
  /** Removing Barcode From CoreData **/
    @IBAction func removeBarcodeAndDetails(_ sender: UIButton) {
 //       model.deleteContext()
        let alert = UIAlertController(title: "êtes-vous sûr?", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Annuler", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "supprimé", style: .cancel, handler: { _ in
           self.model.deleteSql()
            UserDefaults.standard.removeObject(forKey: "barcodeString")
            UserDefaults.standard.removeObject(forKey: "userName")
             self.navigationController?.popToRootViewController(animated: true)
        } ))
        present(alert, animated: true, completion: nil)
    }
    
    func customNavigationBar()
    {
        /**** If Menu Needed Uncomment the Following code ****/
        
//        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
//        menuButton.setBackgroundImage(UIImage(named: "Menu"), for: .normal)
//        menuButton.addTarget(self, action: #selector(sidemenu(sender:)), for: .touchUpInside)
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: menuButton)
        

        self.navigationController?.navigationBar.topItem?.title = "";
        self.navigationItem.title = "Personalisation"
    }
    
    //   MARK: Menu Action
//    @objc func sidemenu(sender:UIButton)
//    {
//        self.openVCSlideMenuButtonPressed(sender as UIButton)
//        print("Slidemenu Clicked")
//    }

}
