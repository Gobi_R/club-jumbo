//
//  ScannerViewController.swift
//  Club Jumbo Privileges
//
//  Created by Gobi R. on 20/04/18.
//

import UIKit
import MTBBarcodeScanner

class ScannerViewController: UIViewController {

    @IBOutlet var viewForScanner: UIView!
    var scanner: MTBBarcodeScanner?
    let defaults = UserDefaults.standard    
    let model = DataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        DispatchQueue.main.async {
//            self.model.deleteContext()
            self.viewForScanner.frame = self.view.bounds
            self.scanner = MTBBarcodeScanner(previewView: self.viewForScanner)
      
        MTBBarcodeScanner.requestCameraPermission (success: { success in
            if success {
                do
                {
                    try self.scanner?.startScanning(resultBlock: { codes in
                        self.startScan()
                        if let scannedCode = codes {
                           for code in scannedCode {
                                print(code.stringValue!)
                                if ( code.stringValue != nil) {
                                    self.scanner?.stopScanning()
                                    self.playSound(done: true)
                                    self.defaults.setValue(code.stringValue, forKey: "barcodeString")
//                                    self.model.save(barcodeStringValue: code.stringValue!)
                                    self.model.insertData()
                                    let nameVc = self.storyboard?.instantiateViewController(withIdentifier: "PersonalisationNameViewController") as! PersonalisationNameViewController
                                    self.navigationController?.pushViewController(nameVc, animated: true)
                                    break
                                    
                                }
                               
                            
                            }
                             
                        }
                    })
                } catch {
                    print("Unnable to Scan")
                }
            } else {
              let controller =  UIAlertController(title: "Scanning Unsuccessfull", message: "This app does not have permission to access the camera", preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))

            }
        })
              }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.scanner?.stopScanning()
        super.viewWillDisappear(true)
    }
    
    func startScan(){
        
        scanner?.didTapToFocusBlock = {(_ point: CGPoint) -> Void in
            print("""
                The user tapped the screen to focus. \
                Here we could present a view at \(NSStringFromCGPoint(point))
                """)
            self.scanner?.captureStillImage({(_ image: UIImage?, _ error: Error?) -> Void in
                print("Image captured. Add a breakpoint here to preview it!")
                print(image!)
            })
        }
        
    }
    
}
