//
//  TableView'sCell.swift
//  Club Jumbo Privileges
//
//  Created by Gobi R. on 19/04/18.
//

import UIKit

class TableView_sCell: UITableViewCell {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var backImageViewMacarte: UIImageView!
    @IBOutlet weak var barcodeBottom: NSLayoutConstraint!
    @IBOutlet weak var barcodeTop: NSLayoutConstraint!
    
    var delteImage = UIImageView()
    var numberLabel = UILabel()
    var userNameLabel = UILabel()
    var id = Int()
    
    // MARK:- Creating Image and labels
    //*** Creating Image and labels for home screen ***//
    
    func createDustBinImage() {
        self.delteImage = UIImageView(frame: CGRect(x: self.contentView.bounds.width - 50, y: self.contentView.bounds.height - 30, width: 20, height: 20))
        delteImage.image = UIImage(named: "delete_grey")
        self.mainImageView.image = constant.barcodeImageFile
        
        self.numberLabel = UILabel(frame: CGRect(x: 0, y: self.contentView.bounds.height-35, width:contentView.bounds.width, height: 35 ))
        numberLabel.adjustsFontForContentSizeCategory = true
        numberLabel.adjustsFontSizeToFitWidth = true
        numberLabel.font = UIFont.boldSystemFont(ofSize: 22)
        numberLabel.textAlignment = .center
        self.contentView.addSubview(numberLabel)
        numberLabel.text = UserDefaults.standard.value(forKey: "barcodeString") as? String
        self.userNameLabel = UILabel(frame: CGRect(x: 0, y: 2, width: self.contentView.bounds.width , height: 30))
        self.userNameLabel.textAlignment = .center
        userNameLabel.font = UIFont.boldSystemFont(ofSize:18)
        self.contentView.addSubview(userNameLabel)
        
        userNameLabel.text = UserDefaults.standard.value(forKey: "userName") as? String
        self.contentView.addSubview(delteImage)
        //        userNameLabel.bringSubview(toFront: self.mainImageView!)
        delteImage.isHidden = false
        numberLabel.isHidden = false
        userNameLabel.isHidden = false
    }
}

class TableViewCell1: UITableViewCell {
    
    @IBOutlet weak var backImageView: UIImageView!
    
   
   
}
//extension UIImage {
//
//    func resize(withWidth newWidth: CGFloat) -> UIImage? {
//
//        let scale = newWidth / self.size.width
//        let newHeight = self.size.height * scale
//        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
//        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
//        let newImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//
//        return newImage
//    }
//}
