//
//  ViewController.swift
//  Club Jumbo Privileges
//
//  Created by Gobi R. on 18/04/18.
//

import UIKit
import CoreData

class ViewController: MenuDisplayControllerViewController, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var magasinsButton: UIButton!
    @IBOutlet weak var contacterButton: UIButton!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let request: NSFetchRequest<BarcodeCoreData> = BarcodeCoreData.fetchRequest()
    var imageArray = [UIImage]()
    var backImageArray = [UIImage]()
    var coreData = [BarcodeCoreData]()
    let model = DataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavigationBar()
        //        imageArray = [UIImage(named: "ma_carte_privelege_bg-1")!,UIImage(named: "shopping_cart_bg-1")!]
       // backImageArray = [UIImage(named: "ma_carte_privelege_bg-2")!, UIImage(named: "shopping_cart_bg-2")!]
        print(imageArray.count)
        tableView.backgroundColor = UIColor.yellow
        magasinsButton.imageView?.contentMode = .scaleToFill
        contacterButton.imageView?.contentMode = .scaleToFill
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        jumboSparLogo()
        //        model.loadContext()
        model.retriveData()
        tableView.reloadData()
        navigationController?.navigationBar.barTintColor = UIColor(red: 254/255, green: 81/255, blue: 82/255, alpha: 1.0)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*CheckEmpty Constent Checks there any values saved in Sqlite*/
        let checkEmpty = model.checkEmptyDatabase()
        if indexPath.row == 0 {
            let cell: TableView_sCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableView_sCell
            cell.backImageViewMacarte.image = UIImage(named: "ma_carte_privelege_bg-2")
            cell.mainImageView.layer.masksToBounds = true
            cell.mainImageView.isUserInteractionEnabled = true
            cell.delteImage.isHidden = true
            cell.numberLabel.isHidden = true
            cell.userNameLabel.isHidden = true
            if checkEmpty != 0 {
                if cell.reuseIdentifier == "Cell" {
                    //                if indexPath.row == 0 {
                    cell.mainImageView?.contentMode = .scaleToFill
                    //              cell1.mainImageView?.frame = CGRect(x: 0, y: 0, width: cell1.contentView.frame.width-5, height: cell1.contentView.frame.height-5)
                    cell.backImageViewMacarte.isHidden = true
                    cell.createDustBinImage()
                    cell.id = indexPath.row
                    cell.barcodeBottom.constant = 30
                    cell.barcodeTop.constant = 30
                    // }
                }} else {
                cell.backImageViewMacarte.isHidden = false
                cell.mainImageView.image = UIImage(named: "ma_carte_privelege_bg-1")
                cell.barcodeBottom.constant = 20
                cell.barcodeTop.constant = 20
            }
            return cell
        } else {
            let cell: TableViewCell1 = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! TableViewCell1
            cell.backImageView.image = UIImage(named: "shopping_cart_bg-2")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.layer.bounds.height/2
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let checkDatabase = model.checkEmptyDatabase()
        let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewJnViewController") as! WebViewJnViewController
        if indexPath.row == 1 {
            webView.url = constant.promotionsURL
            webView.pageTitle = constant.clubJumbo
            navigationController?.pushViewController(webView, animated: true)
        }
            //        else if coreData.count > 0
        else if checkDatabase != 0 {
            if indexPath.row == 0 {
                let removeBarcodeView = storyboard?.instantiateViewController(withIdentifier: "RemoveBarcodeViewController") as! RemoveBarcodeViewController
                navigationController?.pushViewController(removeBarcodeView, animated: true)
            }
        } else {
            let fidelitySignupView = storyboard?.instantiateViewController(withIdentifier: "FidelityCardInput_or_Signup") as! FidelityCardInput_or_Signup
            navigationController?.pushViewController(fidelitySignupView, animated: true)
        }
    }
    
    @IBAction func contactinfoButton(_ sender: UIButton) {
        toWebView(typeURL: constant.contatctFormURl, title: constant.clubJumbo)
    }
    @IBAction func magasinsButton(_ sender: UIButton) {
        toWebView(typeURL: constant.magasinsURL, title: constant.clubJumbo)
    }
    func toWebView(typeURL: String, title: String) {
        
        let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewJnViewController") as! WebViewJnViewController
        if typeURL.isEmpty {
            print("URL is Empty")
        }else {
            webView.url = typeURL
            webView.pageTitle = title
            navigationController?.pushViewController(webView, animated: true)
        }
    }
    func customNavigationBar()
    {
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        backButton.setBackgroundImage(UIImage(named: "Menu"), for: .normal)
        backButton.addTarget(self, action: #selector(sidemenu(sender:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func sidemenu(sender:UIButton)
    {
        self.openVCSlideMenuButtonPressed(sender as UIButton)
        print("Slidemenu Clicked")
    }
    
    //MARK: Logos
    func jumboSparLogo() {
        let jumboLogo = UIImageView(image: UIImage(named: "jumbo"))
        let logo1 = UIBarButtonItem(customView: jumboLogo)
        let sparLogo = UIImageView(image: UIImage(named: "spar"))
        let logo2 = UIBarButtonItem(customView: sparLogo)
        self.navigationItem.setLeftBarButtonItems([logo1,logo2], animated: true)
        jumboLogo.isUserInteractionEnabled = false
        sparLogo.isUserInteractionEnabled = false
        
    }
}


