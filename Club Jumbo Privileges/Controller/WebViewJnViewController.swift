//
//  WebViewJnViewController.swift
//  Club Jumbo Privileges
//
//  Created by Gobi R. on 19/04/18.
//

import UIKit

class WebViewJnViewController: MenuDisplayControllerViewController,UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var spinnerBackView: UIVisualEffectView!
    @IBOutlet weak var spinnerView: UIView!
    
    
    var url = String()
    var pageTitle = String()
    var html = String()
    /* if download url wants */
//    let durl = constant.downloadURL
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        spinnerBackView.layer.cornerRadius = 20
        spinnerBackView.clipsToBounds = true
        backButton()
        webView.delegate = self
        if url.isEmpty {
            loadWith(html: html)
        } else {
            loadWith(url: url)
        }
         customNavigationBar()
        webView.scrollView.bounces = false
        webView.scrollView.showsVerticalScrollIndicator = false
        webView.scrollView.showsHorizontalScrollIndicator = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(yourfunction(notification:)), name: .postNotifi, object: nil)
        
    }
    
    @objc func yourfunction(notification: NSNotification) {
      // print(notification)
        if let getURL = notification.userInfo?["url"] as? String , let getTitle = notification.userInfo?["title"] as? String, let getHtml = notification.userInfo?["html"] as? String {
            var title = String()
           html = getHtml
            title = getTitle
            url = getURL
            if self.url.isEmpty {
                loadWith(html: self.html)
                html = getHtml
                title = getTitle
            } else {
                loadWith(url: self.url)
                url = getURL
                title = getTitle
            }
            pageTitle = title
            customNavigationBar()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .postNotifi, object: nil)
    }
    
    // MARK: -  Web Request
    
    func loadWith(url:String) {
       self.spinner.startAnimating()
        self.spinnerBackView.isHidden = false
        if NetWorkChecker.isConnectedToNetWork() {
            webView.isUserInteractionEnabled = true
            let convertedUrl = URL(string:url)
            let request = URLRequest(url: convertedUrl!)
            webView.loadRequest(request)
        } else {
            let alert = UIAlertController(title: "", message: "S'il vous plait, vérifiez votre connexion internet", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Recommencez", style: .cancel, handler: { (cancel) in
                self.navigationController?.popToRootViewController(animated: true)
            }))
            present(alert, animated: true, completion: {
                self.spinnerBackView.isHidden = true
                self.spinner.stopAnimating()
                self.getCurrentIndex = 0
                
            })
           
        }
    }
    

    var getCurrentIndex: Int!
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("Web Request Success")
        self.spinnerBackView.isHidden = true
        self.spinner.stopAnimating()
         getCurrentIndex = 0

    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        getCurrentIndex = 0
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        spinnerBackView.isHidden = true
        spinner.stopAnimating()
        getCurrentIndex = 0
        print("Error While fetch info from URL:\(error)")
        
    }
    
//    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
//       print(navigationType.rawValue)
//        if navigationType.rawValue != getCurrentIndex{
//             getCurrentIndex = navigationType.rawValue
//            self.spinnerBackView.isHidden = false
//            self.spinner.startAnimating()
//        }else{
//            self.spinnerBackView.isHidden = true
//            self.spinner.stopAnimating()
//            getCurrentIndex = 0
//        }
//       
//    
//        
//        return true
//    }
    
    
    
    func customNavigationBar()
    {
//        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
//        backButton.setBackgroundImage(UIImage(named: "Menu"), for: .normal)
//        backButton.addTarget(self, action: #selector(sidemenu(sender:)), for: .touchUpInside)
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationController?.navigationBar.topItem?.title = "";
        self.navigationItem.title = pageTitle
    }
    
    @objc func sidemenu(sender:UIButton)
    {
      
        self.openVCSlideMenuButtonPressed(sender as UIButton)
       
    }
    
    func loadWith(html: String) {
        self.spinnerBackView.isHidden = false
        self.spinner.startAnimating()
  if NetWorkChecker.isConnectedToNetWork() {
    webView.isUserInteractionEnabled = false
    if constant.docOrHtml == true {
    webView.loadHTMLString(html, baseURL: Bundle.main.bundleURL)
    } else {
        let url = URL(fileURLWithPath: html)
        let urlReq = URLRequest(url: url)
        webView.loadRequest(urlReq)
        self.webView.isUserInteractionEnabled = true
        self.webView.scalesPageToFit = true
    }
    }
  else {
    let alert = UIAlertController(title: "", message: "S'il vous plait, vérifiez votre connexion internet", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Recommencez", style: .cancel, handler: { (cancel) in
        self.navigationController?.popToRootViewController(animated: true)
    }))
     present(alert, animated: true, completion: nil)
    self.spinnerBackView.isHidden = true
    self.spinner.stopAnimating()
        }
    }
}
